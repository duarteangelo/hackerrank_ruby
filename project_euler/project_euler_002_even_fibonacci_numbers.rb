# Project Euler #002: Even Fibonacci numbers
# https://www.hackerrank.com/contests/projecteuler/challenges/euler002/problem

def draw_fibo(n)
  i = 2
  a = [1, 2]
  while a.last < n
    a[i] = a[i - 2] + a[i - 1] if i > 1
    i += 1
  end

  a.pop
  a.select(&:even?).sum
end
