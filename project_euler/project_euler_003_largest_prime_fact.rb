# Project Euler #003: Largest prime factor
# https://www.hackerrank.com/contests/projecteuler/challenges/euler003/problem

def largest_prime_fact(num)
  a = (2..num).reject(&:even?).to_a.unshift(2)

  a = a.reject{ |x| x % 3 == 0 && x != 3 } if num > 3

  a = a.reject{ |x| x % 5 == 0 && x != 5 } if num > 5

  a = a.reject{ |x| x % 7 == 0 && x != 7 } if num > 7

  a
end

largest_prime_fact(10)