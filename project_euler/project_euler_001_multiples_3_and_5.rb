# Project Euler #001: Multiples of 3 and 5
# https://www.hackerrank.com/contests/projecteuler/challenges/euler001/problem

def sum_pa(primeiro_termo, razao, n)
  posicao_ultimo = n / razao
  ultimo_termo = primeiro_termo + razao * (posicao_ultimo - 1)
  ((primeiro_termo + ultimo_termo) * posicao_ultimo) / 2
end

def sum_multiples(n)
  s3 = 0
  s5 = 0
  s15 = 0

  s3 = sum_pa(3, 3, n - 1) if n > 3
  s5 = sum_pa(5, 5, n - 1) if n > 5
  s15 = sum_pa(15, 15, n - 1) if n > 15

  s3 + s5 - s15
end
